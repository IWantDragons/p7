﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XMLTester.Parsing
{
    [Serializable]
    class ParseException : Exception
    {
        public ParseException(string expected, string actual, int lineNumber)
            : base(String.Format("Expected: \"{0}\", found: \"{1}\", at line {2}", expected, actual, lineNumber)) { }

        public ParseException(string message)
            : base(message) { }
    }
}
