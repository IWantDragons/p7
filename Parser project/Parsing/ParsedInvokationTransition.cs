﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data.Types;

namespace XMLTester.SemanticAnalysis
{
    //This struct is used to parse the content of an invokation transition into its constituent parts, so that they are more accesable in the visitMachine method.
    public class ParsedInvokationTransition
    {
        public List<string> input { get; }
        public List<string> output { get; }
        public AST.Behavioural.Operation op { get; }

        public ParsedInvokationTransition(string content)
        {
            // Check if there is an input or output list. That is: [obj1 obj2 ... objn]
            if (content.StartsWith('['))
            {
                var ioNames = Regex.Split(content.Trim('[', ']', ' '), @"\s+").ToList();

                op = new InputOutputOperation(ioNames);
                input = null;
                output = null;
                return;
            }

            //Split the string up into its constituent piececs.
            var match = Regex.Match(
                content,
                @"^((?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*)\s*:\s*([a-zA-Z_][a-zA-Z0-9_]*.(?:[0-9]+|\*).[a-zA-Z_][a-zA-Z0-9_]*|[a-zA-Z_][a-zA-Z0-9_]*|[a-zA-Z_][a-zA-Z0-9_]*\s*(?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*)\s*:\s*((?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*)$");
            input = Regex.Split(match.Groups[1].Value, @"\s+").Where(str => str != "").ToList();
            output = Regex.Split(match.Groups[3].Value, @"\s+").Where(str => str != "").ToList();
            //The invokation is a handler, and we specify which entity it is on.
            if (Regex.IsMatch(match.Groups[2].Value, @"[a-zA-Z_][a-zA-Z0-9_]*.(?:[0-9]+|\*).[a-zA-Z_][a-zA-Z0-9_]*"))
            {
                op = new InvokeEventOperation(match.Groups[2].Value);
            }
            //The invokation is a call to another machine.
            else if (Regex.IsMatch(match.Groups[2].Value, @"[a-zA-Z_][a-zA-Z0-9_]*"))
            {
                op = new InvokeMachineOperation(match.Groups[2].Value);
            }
            //The invokation is a function, which is also defined here.
            else if (Regex.IsMatch(match.Groups[2].Value, @"[a-zA-Z_][a-zA-Z0-9_]*\s*(?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*"))
            {
                List<TypeNode> parameterTypes = new List<TypeNode>();
                List<string> split = Regex.Split(match.Groups[2].Value, @"\s+").ToList();
                //We get the type definition of the function.
                foreach (string param in split.Skip(1))
                {
                    parameterTypes.Add(Intermediate2ASTVisitor.GetTypeFromString(param));
                }
                op = new InvokeFunctionOperation(split[0], parameterTypes);
            }
            else
                throw new Exception("Could not identify invokation");
        }
    }
}
