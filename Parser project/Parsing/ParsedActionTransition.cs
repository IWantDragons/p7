﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XMLTester.AST.Behavioural;

namespace XMLTester.SemanticAnalysis
{
    public class ParsedActionTransition
    {
        public string ActionName { get; }
        public string TimedState { get; }
        public Comparators Cmp { get; }
        public List<Tuple<uint, Units>> Constraints { get; }

        public ParsedActionTransition(string content)
        {
            //Get each individual part. Action name and TimedState can be directly set.
            Match match = Regex.Match(
                content, // Replace since regex cannot handle the colons around handlers
                @"^\s*(:?[_a-zA-Z][_a-zA-Z0-9]*:?)?\s*\[\s*([_a-zA-Z][_a-zA-Z0-9]*)\s*(<|<=|=|>=|>)\s*([0-9]+\s*(?:d|h|m|s|ms|us)\s*)+\]\s*$");
            ActionName = match.Groups[1].Value;
            TimedState = match.Groups[2].Value;
            string cmp = match.Groups[3].Value;
            //Convert string representation into enum.
            if (cmp == "=")
                Cmp = Comparators.Eq;
            else if (cmp == "<=")
                Cmp = Comparators.LessEq;
            else if (cmp == "<")
                Cmp = Comparators.Less;
            else if (cmp == ">=")
                Cmp = Comparators.GreaterEq;
            else if (cmp == ">")
                Cmp = Comparators.Greater;
            else
                throw new Exception("Unknown comparator " + cmp);
            //Get list of timing constraints. This is a list of integer values and time units (stored as a enum). Since the time unit is stored as a string, we have to translate it.
            Constraints = new List<Tuple<uint, Units>>();
            foreach (string constraint in match.Groups[4].Captures.Select(cap => cap.Value.Trim()))
            {
                Match pair = Regex.Match(constraint, @"^([0-9]+)[ ]*(:d|h|m|s|ms|us)$");
                Units unit = pair.Groups[2].Value switch
                {
                    "d" => Units.Day,
                    "h" => Units.Hour,
                    "m" => Units.Minute,
                    "s" => Units.Second,
                    "ms" => Units.Millisecond,
                    "us" => Units.Microsecond,
                    _ => throw new Exception("Unknown unit " + pair.Groups[2].Value),
                };
                Constraints.Add(new Tuple<uint, Units>(uint.Parse(pair.Groups[1].Value), unit));
            }
        }
    }
}
