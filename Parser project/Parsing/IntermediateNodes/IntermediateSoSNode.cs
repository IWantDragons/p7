﻿using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class IntermediateSoSNode : INode
    {
        public List<IntermediateSoSNode> Dependencies { get; set; }
        public ComponentDiagramNode System { get; set; }


        public IntermediateSoSNode()
        {
            Dependencies = new List<IntermediateSoSNode>();
        }


        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitSoSNode(this);
        }
    }
}
