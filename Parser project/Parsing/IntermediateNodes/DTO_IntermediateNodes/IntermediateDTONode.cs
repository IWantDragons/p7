﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class IntermediateDTONode : DTOBaseNode
    {
        public string DTOName { get; }
        public string DTOID { get; }
        public string BaseDTOID { set; get; }

        public List<AttributeNode> Fields { get; }
        public List<ConstraintNode> Constraints { get; }


        public IntermediateDTONode(string name, string _dtoid)
        {
            DTOName = name;
            DTOID = _dtoid;
            Fields = new List<AttributeNode>();
            Constraints = new List<ConstraintNode>();
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitDTONode(this);
        }
    }
}
