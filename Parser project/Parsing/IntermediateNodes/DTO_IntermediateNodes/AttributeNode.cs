﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class AttributeNode : DTOBaseNode
    {
        public IntermediateTypeNode type;
        public string attributeName { set; get; }
        public string Visibility { get; }

        public AttributeNode(string _name, string _visibility, IntermediateTypeNode _type)
        {
            type = _type;
            Visibility = _visibility;
            attributeName = _name;
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitAttributeNode(this);
        }
    }

}
