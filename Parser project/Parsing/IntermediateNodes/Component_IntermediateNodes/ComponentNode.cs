﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ComponentNode : ComponentBaseNode
    {
        public string PapID { get; }
        public string Name { get; }
        public string Count { get; }

        public EntityNode EntityDecl { get; set; }

        public List<string> Ports { get; }
        public List<ComponentNode> Constituents { get; }
        public List<ConnectorNode> Links { get; }


        public ComponentNode(string id, string name, string count)
        {
            PapID = id;
            Name = name;
            Count = count;

            Ports = new List<string>();
            Constituents = new List<ComponentNode>();
            Links = new List<ConnectorNode>();
        }


        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitComponentNode(this);
        }
    }
}
