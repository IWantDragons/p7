﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ConnectorNode : ComponentBaseNode
    {
        public string Mult { get; }
        public string Type { get; }
        public List<string> EndIDs { get; }


        public ConnectorNode(string _mult, string _type, List<string> _ends)
        {
            Mult = _mult;
            Type = _type;
            EndIDs = _ends;
        }


        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitConnectorNode(this);
        }
    }
}
