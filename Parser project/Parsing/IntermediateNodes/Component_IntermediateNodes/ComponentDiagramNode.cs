﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ComponentDiagramNode : ComponentBaseNode
    {
        public List<ComponentNode> Components { get; }
        public List<ConnectorNode> Links { get; }
        public List<string> Ports { get; }
        public string Name { get; }

        public ComponentDiagramNode(string name)
        {
            Name = name;
            Components = new();
            Links = new();
            Ports = new();
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitComponentDiagramNode(this);
        }
    }
}
