﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class PropertyNode : INode
    {
        public string PapID { get; set; }

        public string Signature { get; }
        public string Visibility { get; }
        public IntermediateTypeNode Type { get; }


        public PropertyNode(string papID, string signature, string visibility, IntermediateTypeNode type)
        {
            PapID = papID;
            Signature = signature;
            Visibility = visibility;
            Type = type;
        }


        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitPropertyNode(this);
        }
    }
}
