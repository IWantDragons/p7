using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class AssociationNode : PackagedElementNode, INode
    {
        public string ID { get; }
        public string Name { get; }
        public string Visibility { get; }

        /// <summary>
        /// The "Containing Class" contains some number of "ChildClass"
        /// </summary>
        public string ContainingClassID { get; set; }
        public IntermediateTypeNode ChildClassType { get; set; }
        public List<ConstraintNode> Constraints { get; }

        public AssociationNode(string id, string name, string visibility)
        {
            ID = id;
            Name = name;
            Visibility = visibility;

            ContainingClassID = null;
            ChildClassType = null;
            Constraints = new List<ConstraintNode>();
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitAssociationNode(this);
        }
    }
}
