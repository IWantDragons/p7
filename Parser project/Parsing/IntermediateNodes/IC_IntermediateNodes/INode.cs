﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public interface INode
    {
        T Accept<T>(IIntermediateVisitor<T> visitor);
    }
}
