﻿using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class OperationNode : INode
    {
        public string PapID { get; }
        public string Signature { get; }
        public List<ParameterNode> Parameters { get; }
        public string Visibility { get; }


        public OperationNode(string id, string signature, string visibility)
        {
            PapID = id;
            Signature = signature;
            Visibility = visibility;
            Parameters = new List<ParameterNode>();
        }
        

        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitOperationNode(this);
        }
    }
}
