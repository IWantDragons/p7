﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ParameterNode : INode
    {
        public string PapID { get; set; }

        public string Signature { get; }
        public IntermediateTypeNode Type { get; }
        public bool IsReturnType { get; }

        public ParameterNode(string papID, string signature, IntermediateTypeNode type, bool isReturnType)
        {
            PapID = papID;
            Signature = signature;
            Type = type;
            IsReturnType = isReturnType;
        }

        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitParameterNode(this);
        }
    }
}
