﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ConnectionEndNode : INode
    {        
        public string RelatedClassID { get; }
        public string AggregationType { get; }
        public string Lower { get; }
        public string Upper { get; }

        public ConnectionEndNode(string _relatedClassID, string _aggregation, string _lower, string _upper)
        {
            RelatedClassID = _relatedClassID;
            AggregationType = _aggregation;
            Lower = _lower;
            Upper = _upper;
        }

        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitConnectionEndNode(this);
        }
    }
}
