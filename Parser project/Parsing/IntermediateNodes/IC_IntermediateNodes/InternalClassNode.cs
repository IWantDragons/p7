﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class InternalClassNode : INode
    {
        public List<PackagedElementNode> Elements { get; }

        public InternalClassNode() {
            Elements = new List<PackagedElementNode>();
        }

        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitInternalClassNode(this);
        }
    }
}
