﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class StateNode : BehaviorBaseNode
    {
        public String StateName { get; }

        public string ID { get; }

        public StateNode(string stateName, string _id)
        {
            StateName = stateName;
            ID = _id;
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            throw new NotImplementedException();
        }
    }
}
