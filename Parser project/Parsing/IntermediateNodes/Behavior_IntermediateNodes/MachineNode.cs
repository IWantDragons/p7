﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class MachineNode : BehaviorBaseNode
    {

        public List<StateNode> States { get; }

        public List<TransitionNode> Transitions { get; }

        public string MachineName { get; }

        public string ID { get; }

        public MachineNode(string _name, string _id)
        {
            MachineName = _name;
            ID = _id;
            States = new List<StateNode>();
            Transitions = new List<TransitionNode>();
        }

        public MachineNode(MachineNode mac)
        {
            MachineName = mac.MachineName;
            ID = mac.ID;
            States = mac.States;
            Transitions = mac.Transitions;
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitMachineNode(this);
        }
    }
}
