﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.IntermediateNodes;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ControllerMachineNode : MachineNode
    {
        public string Content { set; get; }

        public ControllerMachineNode(MachineNode mac, string _eventDecls) : base(mac)
        {
            Content = _eventDecls;
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitControllerNode(this);
        }
    }
}
