﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class TransitionNode : BehaviorBaseNode
    {
        public string Source { get; }

        public string Destination { get; }

        public string Content { get; }

        public string TransitionID { get; }

        public TransitionNode(string _name, string _id, string _source, string _target)
        {
            Content = _name;
            TransitionID = _id;
            Source = _source;
            Destination = _target;
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            throw new NotImplementedException();
        }
    }
}
