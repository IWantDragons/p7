﻿using System;

namespace XMLTester.Symbols
{
    public class Symbol
    {
        private static int _id = 0;
        private int ID { get; set; }
        private Type Type { get; set; }
        public string Name { get; set; }

        public Symbol(Type type, String name)
        {
            Type = type;
            Name = name;
            ID = ++_id;
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }

        public override string ToString()
        {
            return $"{{{Type}:{Name}}}";
        }
    }
}
