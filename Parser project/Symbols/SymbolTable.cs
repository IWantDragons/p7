﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;

namespace XMLTester.Symbols
{
    public class SymbolTable
    {
        Dictionary<string, Scope> scopes = new Dictionary<string, Scope>();

        public Symbol GetSymbol(string scopeName, string symbolName)
        {
            if (!scopes.ContainsKey(scopeName))
                throw new InvalidOperationException($"Could not access the scope <{scopeName}>");
           
            return GetSymbol(scopes[scopeName], symbolName);
        }

        public Symbol GetSymbol(Scope scope, string symbolName)
        {
            if (scope.ContainsSymbol(symbolName))
                return scope.GetSymbolWithName(symbolName);
            else
            {
                if (scope.GetSymbolWithName(symbolName) == null)
                    throw new InvalidOperationException($"No variable with the name: <{symbolName}> in the chain of scopes");
                else
                    return GetSymbol(scope.Name, symbolName);
            }
        }

        public void AddScope(Scope scope)
        {
            if(scopes.ContainsKey(scope.Name))
                throw new InvalidOperationException($"Tried reading an already existing scope: {scope}");
            
            scopes.Add(scope.Name, scope);
        }
        
    }
}
