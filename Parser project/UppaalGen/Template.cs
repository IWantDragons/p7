﻿using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.SymbolTables;

namespace XMLTester.UppaalGen
{
    public class Template
    {
        public string Name { get; }
        public string Parameter { get; }
        public IReadOnlyList<string> Declarations { get; }
        public IReadOnlyList<Location> Locations { get; }
        public Location Init { get; }
        public IReadOnlyList<Transition> Transitions { get; }

        public uint InstanceCount { get; }
        public SymbolTable AssociatedScope { get; }
        public IReadOnlyList<string> SyncronizedEntityIDs { get; }
        public IReadOnlyList<string> OwnedChannels { get; }

        public Template(
            string name, uint instanceCount,
            IReadOnlyList<string> declarations, IReadOnlyList<Location> locations, Location init, IReadOnlyList<Transition> transitions,
            SymbolTable associatedScope, IReadOnlyList<string> syncronizedEntityIDs, IReadOnlyList<string> ownedChannels)
        {
            Name = name;
            Parameter = $"const cnt_{instanceCount} instance_id";
            InstanceCount = instanceCount;

            Declarations = declarations;
            Locations = locations ?? throw new ArgumentNullException(nameof(locations));
            Init = init ?? throw new ArgumentNullException(nameof(init));
            Transitions = transitions ?? throw new ArgumentNullException(nameof(transitions));

            AssociatedScope = associatedScope ?? throw new ArgumentNullException(nameof(associatedScope));
            SyncronizedEntityIDs = syncronizedEntityIDs ?? throw new ArgumentNullException(nameof(syncronizedEntityIDs));
            OwnedChannels = ownedChannels ?? throw new ArgumentNullException(nameof(ownedChannels));

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("The name of a template cannot be blank");

            if (instanceCount == 0)
                throw new ArgumentException("The count of a template must be positive");

            if (locations.Count == 0)
                throw new ArgumentException("The list of locations cannot be empty");

            if (!locations.Contains(init))
                throw new ArgumentException("The initial location provided is not in the list of locations");

            if (transitions.Count == 0)
                throw new ArgumentException("The list of transitions cannot be empty");
        }
    }
}
