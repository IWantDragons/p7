﻿using System;
namespace XMLTester.UppaalGen
{
    public class Query
    {
        public string Formula { get; }
        public string Comment { get; }

        public Query(string formula, string comment)
        {
            Formula = formula;
            Comment = comment;

            if (string.IsNullOrWhiteSpace(formula))
                throw new ArgumentException("The query cannot be blank", formula);
        }

        public Query() { }
    }
}
