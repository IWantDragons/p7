﻿using System;
using System.Collections.Generic;

namespace XMLTester.UppaalGen
{
    public class Location
    {
        public const int TEXT_OFFSET_X = 17;
        public const int NAME_OFFSET_Y = -27;

        private List<string> _invariants = new List<string>();

        public uint ID { get; }
        public string Name { get; }
        public bool Urgent { get; set; }
        public string Invariant { get => string.Join(" || ", _invariants); }

        public Location(uint id, string name)
        {
            ID = id;
            Name = name.Replace('.', '_');
        }

        public void AddInvatiant(string invariant)
        {
            _invariants.Add(invariant);
        }
    }
}
