﻿using System;
namespace XMLTester.UppaalGen
{
    public class PositionedElement
    {
        public int X { get; }
        public int Y { get; }
        public string Body { get; set; }

        public PositionedElement(int x, int y, string body)
        {
            X = x;
            Y = y;
            Body = body;
        }
    }
}
