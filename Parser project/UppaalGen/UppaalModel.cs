﻿using System;
namespace XMLTester.UppaalGen
{
    public class UppaalModel
    {
        public string FullModel { get; }
        public string Queries { get; }

        public UppaalModel(string fullModel, string queries)
        {
            FullModel = fullModel;
            Queries = queries;
        }
    }
}
