﻿using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.AST.Behavioural;
using XMLTester.Exceptions;
using XMLTester.Extensions;
using XMLTester.SymbolTables;

namespace XMLTester.UppaalGen.Builder
{
    public class UppaalMachine
    {
        private int _templateNumber;
        Dictionary<string, int> _machineNameToIDMap;

        private static uint _nextLocationID = 0;

        private Dictionary<string, Location> _stateToLocationMap = new Dictionary<string, Location>();
        private Dictionary<string, Location> _machineToStartLocationMap = new Dictionary<string, Location>();
        private Dictionary<string, Location> _machineToEndLocationMap = new Dictionary<string, Location>();

        public Location Init { get; }
        public List<Location> Locations { get; } = new List<Location>();
        public List<Transition> Transitions { get; } = new List<Transition>();
        public HashSet<string> SyncronizedEntityIDs { get => Transitions.Select(t => t.SyncTarget?.GetFullyQualifiedName().Replace('.', '_')).NotNull().ToHashSet(); }
        public HashSet<string> OwnedChannels { get; } = new HashSet<string>();


        public UppaalMachine(string initialStateName, int templateNumber, Dictionary<string, int> machineNameToIdMap)
        {
            // Since invariants on the first UPPAAL location does not register/work, we insert
            // a dummy starting/Init location, such that the invariants on the ACE-start-node works.
            Init = new Location(_nextLocationID++, "Init") { Urgent = true };
            Locations.Add(Init);
            Location start = GetOrCreateControllerLocation(initialStateName);
            Transitions.Add(new Transition(Init.ID, start.ID, null, null, null, null, null, null));

            _templateNumber = templateNumber;
            _machineNameToIDMap = machineNameToIdMap;
        }


        public Location GetOrCreateControllerLocation(string stateName)
        {
            return GetOrCreateLocation(stateName, $"CTRL.{stateName}", _stateToLocationMap);
        }

        public Location GetOrCreateCommunicatorLocation(string eventName, string machineName, string stateName, bool createAsNewStartLocation)
        {
            Location location = GetOrCreateLocation(stateName, $"COM.{eventName}.{machineName.Split(' ').First()}.{stateName}", _stateToLocationMap);

            if (createAsNewStartLocation)
            {
                string name = $"{eventName}.{machineName.Split(' ').First()}";
                if (!_machineToStartLocationMap.TryAdd(name, location))
                    throw new ArgumentException($"There is already a start state for: '{name}'", $"params: {eventName} & {machineName.Split(' ').First()}");
            }

            return location;
        }

        public Location TryGetMachineStartNode(string eventName, string machineName)
        {
            string name = $"{eventName}.{machineName.Split(' ').First()}";

            if (_machineToStartLocationMap.TryGetValue(name, out Location location))
                return location;

            return null;
        }

        public Location GetOrCreateMachineEndLocation(string eventName, string machineName)
        {
            return GetOrCreateLocation($"{machineName.Split(' ').First()}.end", $"{eventName}.{machineName.Split(' ').First()}", _machineToEndLocationMap);
        }

        private Location GetOrCreateLocation(string stateName, string fullName, Dictionary<string, Location> map)
        {
            if (map.TryGetValue(fullName, out Location location))
                return location;

            Location newLocation = new Location(_nextLocationID, stateName);
            Locations.Add(newLocation);
            map[fullName] = newLocation;

            _nextLocationID++;

            return newLocation;
        }


        public Transition CreateActionTransition(
            Location start, Location end, ActionTransitionNode actTransition, ActionTypes actionType, ActionConversionInfo conversionInfo)
        {
            string select = null, synchronization = null;
            List<string> guards = new(), assignments = new();

            string actionName = actTransition.ActionName;

            if (actionType == ActionTypes.Error)
                throw new ArgumentException("Not supported: Error transitions");
            else if (actionType == ActionTypes.Delay)
            {
                string timedState = actTransition.TimedStateID;
                Comparators comparator = actTransition.Comparator;
                var durations = actTransition.Durations;

                guards.Add($"clk_{conversionInfo.StateToClockMap[timedState]} {GetComparatorString(comparator)} {conversionInfo.TimeConstraint}");
                assignments.AddRange(conversionInfo.ClocksToReset.Select(clock => $"clk_{clock} = 0"));
            }
            else if (actionType == ActionTypes.StartInitiator || actionType == ActionTypes.StartHandler)
            {
                if (actionType == ActionTypes.StartHandler)
                {
                    select = $"other : id_all";
                    synchronization = $"bgn_{_templateNumber}_{actionName}[other]?";
                    assignments.Add("mostRecentCaller = other");

                    // We just save the general form of the uppaal channel, since the template must have both "begin" and "end".
                    OwnedChannels.Add($"{_templateNumber}_{actionName}");
                }

                if (_machineNameToIDMap.TryGetValue(actionName, out int machineID))
                    assignments.Add($"PushStack({machineID}, {conversionInfo.CallNumber})");
                else if (actionType != ActionTypes.StartHandler)
                    throw new UppaalGeneratorException($"There is no machine or machine ID for non-handler event '{actionName}'");
            }
            else if (actionType == ActionTypes.EndInitiator || actionType == ActionTypes.EndHandler)
            {
                if (actionType == ActionTypes.EndHandler)
                {
                    synchronization = $"end_{_templateNumber}_{actionName}[mostRecentCaller]!";
                    assignments.Add("mostRecentCaller = -1");
                }

                string timedState = actTransition.TimedStateID;
                guards.Add($"clk_{conversionInfo.StateToClockMap[timedState]} {GetComparatorString(actTransition.Comparator)} {conversionInfo.TimeConstraint}");
                assignments.AddRange(conversionInfo.ClocksToReset.Select(clock => $"clk_{clock} = 0"));

                if (_machineNameToIDMap.ContainsKey(actionName))
                {
                    guards.Add($"{conversionInfo.CallNumber} == GetReturn()");
                    assignments.Add("PopStack()");
                }
            }

            Transition newTransition = new Transition(
                start.ID, end.ID,
                select, string.Join(" && ", guards).NullIfBlank(), synchronization, string.Join(", ", assignments).NullIfBlank(),
                synchronization != null ? $"{_templateNumber}_{actionName}" : null, null
            );
            Transitions.Add(newTransition);

            return newTransition;
        }

        private string GetComparatorString(Comparators comparator)
        {
            return comparator switch
            {
                Comparators.Less => "<",
                Comparators.LessEq => "<=",
                Comparators.Eq => "==",
                Comparators.GreaterEq => ">=",
                Comparators.Greater => ">",
                _ => throw new ArgumentException($"Unknown comparator: '{comparator}'", nameof(comparator))
            };
        }


        // This is going to need info about the "Template number" for the syncronized "thing". Perhaps store in Clock-/Conversion-Info
        public Transition CreateInvocationTransition(
            Location start, Location end, InvocationTransitionNode ivkTransition, InvocationTypes invocationType, InvocationConversionInfo conversionInfo, SymbolTable syncTarget)
        {
            string select = null, synchronization = null;
            List<string> guards = new(), assignments = new();

            string handlerName = (ivkTransition?.InvokationOperation as InvokeEventOperation)?.HandlerName;

            if (invocationType == InvocationTypes.StartMachine)
            {
                string invokationName = ivkTransition.InvokationOperation.InvokationName;
                guards.Add($"!StackContainsCall({_machineNameToIDMap[invokationName]})");
                assignments.Add($"PushStack({_machineNameToIDMap[invokationName]}, {conversionInfo.CallNumber})");
            }
            else if (invocationType == InvocationTypes.EndMachine)
            {
                guards.Add($"{conversionInfo.CallNumber} == GetReturn()");
                assignments.Add("PopStack()");
            }
            else if (invocationType == InvocationTypes.StartHandler)
            {
                synchronization = $"bgn_{conversionInfo.SyncTemplateID}_{handlerName}[id]!";
            }
            else if (invocationType == InvocationTypes.EndHandler)
            {
                synchronization = $"end_{conversionInfo.SyncTemplateID}_{handlerName}[id]?";
            }
            else if (invocationType == InvocationTypes.Blank)
            { } // Blank on purpose

            Transition newTransition = new Transition(
                start.ID, end.ID,
                select, string.Join(" && ", guards).NullIfBlank(), synchronization, string.Join(", ", assignments).NullIfBlank(),
                synchronization != null ? $"{conversionInfo.SyncTemplateID}_{handlerName}" : null, syncTarget
            );
            Transitions.Add(newTransition);

            return newTransition;
        }
    }

    public enum ActionTypes
    {
        Delay, StartInitiator, EndInitiator, StartHandler, EndHandler, Error
    }

    public enum InvocationTypes
    {
        StartMachine, EndMachine, StartHandler, EndHandler, Blank
    }
}
