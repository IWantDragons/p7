﻿using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.AST;
using XMLTester.AST.Structural;
using XMLTester.Exceptions;

namespace XMLTester.SymbolTables
{
    public class SymbolTable
    {
        private readonly Dictionary<string, SymbolTable> _scopes = new Dictionary<string, SymbolTable>();

        private readonly Dictionary<string, Symbol> _symbols = new Dictionary<string, Symbol>();

        private readonly HashSet<Propagation> _propagatedSymbols = new HashSet<Propagation>();


        public IReadOnlyDictionary<string, SymbolTable> Scopes { get => _scopes; }

        public IReadOnlyDictionary<string, Symbol> Symbols { get => _symbols; }

        public IReadOnlySet<Propagation> PropagatedSymbols { get => _propagatedSymbols; }


        public string Name { get; }

        public SymbolTable Parent { get; }

        public ScopeTypes ScopeType { get; }

        public IASTNode ASTNode { get; set; }


        public SymbolTable(string name, SymbolTable parent, ScopeTypes scopeType)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Parent = parent ?? throw new ArgumentNullException(nameof(parent));
            ScopeType = scopeType;
        }

        public SymbolTable(string name, ScopeTypes scopeType)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            ScopeType = scopeType;
        }


        public SymbolTable GetScope(string scopeName)
        {
            SymbolTable currentScope = this;

            foreach (string scopeNamePart in scopeName.Split('.').SkipWhile(scope => scope == Name && ScopeType == ScopeTypes.SoS)) // We can skip the first part if it is the current scope and the current scope is a SoS.
                if (!currentScope._scopes.TryGetValue(scopeNamePart, out currentScope))
                {
                    foreach(var table in Scopes.Where(scope => scope.Value.ScopeType == ScopeTypes.Constituent))
                    {
                        if (table.Value._scopes.TryGetValue(scopeName, out currentScope))
                            return currentScope;
                    }

                    return Parent?.GetScope(scopeName);
                }

            return currentScope;
        }

        public Symbol GetSymbol(string symbolName)
        {
            string[] scopeNameParts = symbolName.Split('.');
            string symbolNamePart = scopeNameParts[scopeNameParts.Length - 1];
            scopeNameParts = scopeNameParts[0..^1];

            SymbolTable currentScope = this;
            foreach (string scopeNamePart in scopeNameParts.SkipWhile(scope => scope == Name && ScopeType == ScopeTypes.SoS)) // We can skip the first part if it is the current scope and the current scope is a SoS.
                if (!currentScope._scopes.TryGetValue(scopeNamePart, out currentScope))
                    return Parent?.GetSymbol(symbolName);

            if (currentScope._symbols.TryGetValue(symbolNamePart, out Symbol symbol))
                return symbol;

            return Parent?.GetSymbol(symbolName);
        }

        public Symbol GetPropagatedSymbol(string symbolName)
        {
            foreach(Propagation prop in _propagatedSymbols)
            {
                if (prop.Symbol.Name == symbolName)
                    return prop.Symbol;
            }
            return null;
        }

        public bool HasSymbol(string DTOName)
        {
            if (_symbols.ContainsKey(DTOName))
                return true;
            else
                foreach (SymbolTable Table in _scopes.Values)
                {
                    if (Table.HasSymbol(DTOName))
                        return true;
                }
            return false;
        }

        public SymbolTable NewScope(string scopeName, ScopeTypes scopeType)
        {
            if (scopeName.Contains('.'))
                throw new ArgumentException("The name of a scope cannot contain a DOT", nameof(scopeName));

            SymbolTable table = new SymbolTable(scopeName, this, scopeType);
            _scopes.Add(scopeName, table);

            return table;
        }

        public Symbol NewSymbol(string symbolName, SymbolTypes symbolType)
        {
            if (_symbols.ContainsKey(symbolName))
                return null;
            if (symbolName.Contains('.'))
                throw new ArgumentException("The name of a symbol cannot contain a DOT", nameof(symbolName));

            Symbol symbol = new Symbol(symbolName, this, symbolType);
            _symbols.Add(symbolName, symbol);

            return symbol;
        }


        public bool PropagateSymbol(Symbol symbol, List<SymbolTable> propagationSources)
        {
            return _propagatedSymbols.Add(new Propagation(symbol, propagationSources));
        }


        public string GetFullyQualifiedName()
        {
            return Parent == null ? Name : Parent.GetFullyQualifiedName() + '.' + Name;
        }

        public List<string> Validate()
        {
            // TODO: CHECK TO SEE IF FINISHED LATER
            List<string> errors = new List<string>();

            if (ASTNode == null)
                errors.Add($"The scope '{GetFullyQualifiedName()}' has no associated AST node");

            foreach (SymbolTable tables in _scopes.Values) // Validate only allowed scopes are here
                errors.AddRange(tables.Validate());

            foreach (Symbol symbol in _symbols.Values) // Validate only allowed symbols are here
                errors.AddRange(symbol.Validate());

            return errors;
        }
    }

    public enum ScopeTypes {
        SoS, Constituent, Entity, Communicator, Machine
    }

    public class Propagation
    {
        public Symbol Symbol { get; }

        public List<SymbolTable> PropagationSources { get; }


        public Propagation(Symbol symbol, List<SymbolTable> propagationSources)
        {
            Symbol = symbol;
            PropagationSources = propagationSources;
        }


        public override bool Equals(object obj)
        {
            return obj is Propagation propagation &&
                   EqualityComparer<Symbol>.Default.Equals(Symbol, propagation.Symbol) &&
                   EqualityComparer<List<SymbolTable>>.Default.Equals(PropagationSources, propagation.PropagationSources);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Symbol, PropagationSources);
        }
    }
}
