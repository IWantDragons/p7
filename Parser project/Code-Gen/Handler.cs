﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLTester.Code_Gen
{
    public class Handler
    {
        public string HandlerName { get; }
        public IReadOnlyList<string> Input { get; }
        public IReadOnlyList<string> Output { get; }

        public Handler(string _name, IReadOnlyList<string> _input, IReadOnlyList<string> _output)
        {
            HandlerName = _name;
            Input = _input;
            Output = _output;
        }
    }
}
