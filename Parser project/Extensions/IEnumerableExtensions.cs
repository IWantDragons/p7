﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XMLTester.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> NotNull<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Where(val => val != null);
        }
    }
}
