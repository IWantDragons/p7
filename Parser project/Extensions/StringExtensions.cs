﻿using System;
namespace XMLTester.Extensions
{
    public static class StringExtensions
    {
        public static string NullIfBlank(this string str)
        {
            return string.IsNullOrEmpty(str) ? null : str;
        }
    }
}
