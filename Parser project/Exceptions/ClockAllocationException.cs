﻿using System;
namespace XMLTester.Exceptions
{
    public class ClockAllocationException : Exception
    {
        public ClockAllocationException(string timedState, uint localClock, uint nextClock)
            : base($"The clock allocated for state '{timedState}' is allocated to '{localClock}' in one path, but to '{nextClock}' in another path")
        { }
    }
}
