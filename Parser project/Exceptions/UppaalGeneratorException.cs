﻿using System;
namespace XMLTester.Exceptions
{
    public class UppaalGeneratorException : Exception
    {
        public UppaalGeneratorException(string message) : base(message)
        { }
    }
}
