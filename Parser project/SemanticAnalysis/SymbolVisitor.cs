using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.IntermediateNodes;
using XMLTester.Symbols;

namespace XMLTester.SemanticAnalysis
{
    class SymbolVisitor : IIntermediateVisitor<INode>
    {
        public Symbols.SymbolTable SymbolTable;
        public Stack<Scope> Scopes = new Stack<Scope>();
        public int StmtsCount = 0;

        public SymbolVisitor(Symbols.SymbolTable SymbolTable)
        {
            this.SymbolTable = SymbolTable;
        }

        private void OpenScope(String Name)
        {
            Scope Scope;
            if (Name.Equals("Global"))
                Scope = new Scope(Name, null);
            else
                Scope = new Scope(Name, Scopes.Peek());
            Scopes.Push(Scope);
            SymbolTable.AddScope(Scope);
        }

        private void CloseScope(String Name)
        {
            Scope Scope = Scopes.Pop();
            if (!Scope.Name.Equals(Name))
                throw new InvalidOperationException($"Tried to close a scope that does not exist.\nTried to pop {Name} but head was {Scope.Name}");
        }

        #region IC_IntermediateNodes
        public INode VisitAssociationNode(AssociationNode node)
        {
            string name = node.ID;
            List<ConstraintNode> constraints = node.Constraints;

            OpenScope(name);

            //node.ParentClassID.Accept(this);
            node.ChildClassType.Accept(this);
            
            foreach (ConstraintNode item in constraints)
            {
                item.Accept(this);
            }

            CloseScope(name);

            return node;
        }

        public INode VisitClassNode(ClassNode node)
        {
            string name = node.PapID;
            Symbol symbol;
            List<PropertyNode> variables = node.Variables;
            List<OperationNode> methods = node.Methods;
            List<ConstraintNode> constraints = node.Constraints;

            symbol = new Symbol(node.GetType(), node.PapID);
            Scopes.Peek().AddSymbol(symbol);

            OpenScope(name);

            foreach(PropertyNode item in variables)
            {
                item.Accept(this);
            }

            foreach (OperationNode item in methods)
            {
                item.Accept(this);
            }
                
            foreach(ConstraintNode item in constraints)
            {
                item.Accept(this);
            }

            CloseScope(name);
            return node;
        }

        public INode VisitConnectionEndNode(ConnectionEndNode node)
        {
            Symbol symbol;

            symbol = new Symbol(node.GetType(), node.AggregationType);
            Scopes.Peek().AddSymbol(symbol);

            return node;
        }

        public INode VisitConstraintNode(ConstraintNode node)
        {
            Symbol symbol;

            symbol = new Symbol(node.GetType(), node.ID);
            Scopes.Peek().AddSymbol(symbol);

            return node;
        }

        public INode VisitInternalClassNode(InternalClassNode node)
        {
            List<PackagedElementNode> elements = node.Elements;
            OpenScope("Global");
            foreach(PackagedElementNode item in elements)
            {
                item.Accept(this);
            }
            CloseScope("Global");
            return node;
        }

        public INode VisitOperationNode(OperationNode node)
        {
            string name = node.PapID;
            Symbol symbol = new Symbol(node.GetType(), node.PapID);
            List<ParameterNode> parameters = node.Parameters;
            Scopes.Peek().AddSymbol(symbol);

            OpenScope(name);

            foreach(ParameterNode item in parameters)
            {
                item.Accept(this);
            }

            CloseScope(name);

            return node;
        }

        //COMMENT: Packaged element is an abstract class.
        //public INode VisitPackagedElementNode(PackagedElementNode node)
        //{
        //    throw new NotImplementedException();
        //}

        public INode VisitParameterNode(ParameterNode node)
        {
            Symbol symbol = new Symbol(node.GetType(), node.PapID);
            Scopes.Peek().AddSymbol(symbol);

            return node;
        }

        public INode VisitPropertyNode(PropertyNode node)
        {
            Symbol symbol = new Symbol(node.GetType(), node.Signature);
            Scopes.Peek().AddSymbol(symbol);

            return node;
        }

        public INode VisitTypedNode(TypedNode node)
        {
            throw new NotImplementedException("This was not used for anything during implementation of the visitor");
        }
        #endregion

        #region Behavior_IntermediateNodes

        public INode VisitEntityBehaviorNode(EntityBehavior node)
        {
            throw new NotImplementedException();
        }

        public INode VisitFinalStateNode(FinalStateNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitInitialStateNode(InitialStateNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitMachineNode(MachineNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitStateNode(StateNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitTransitionNode(TransitionNode node)
        {
            throw new NotImplementedException();
        }

        //public INode VisitModelNode(ModelNode node)
        //{
        //    throw new NotImplementedException();
        //}

        //public INode VisitRegionNode(RegionNode node)
        //{
        //    throw new NotImplementedException();
        //}

        //public INode VisitStateChartMachineNode(StateChartMachineNode node)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #region Component_IntermediateNodes

        public INode VisitComponentDiagramNode(ComponentDiagramNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitComponentNode(ComponentNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitConnectorNode(ConnectorNode node)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region DTO_IntermediateNodes
        public INode VisitAttributeNode(AttributeNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitDTONode(IntermediateDTONode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitTypeNode(IntermediateTypeNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitSoSNode(IntermediateSoSNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitEntityNode(EntityNode node)
        {
            throw new NotImplementedException();
        }

        public INode VisitControllerNode(ControllerMachineNode node)
        {
            throw new NotImplementedException();
        }
        #endregion
        //public INode VisitDiagramNode(InternalClassNode node)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
