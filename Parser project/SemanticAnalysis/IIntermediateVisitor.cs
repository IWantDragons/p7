using System;
using System.Collections.Generic;
using XMLTester.IntermediateNodes;

namespace XMLTester.SemanticAnalysis
{
    public interface IIntermediateVisitor<T>
    {
        #region behaviorNodes
        T VisitEntityBehaviorNode(EntityBehavior node);
        T VisitFinalStateNode(FinalStateNode node);
        T VisitInitialStateNode(InitialStateNode node);
        T VisitMachineNode(MachineNode node);
        T VisitControllerNode(ControllerMachineNode node);
        T VisitStateNode(StateNode node);
        T VisitTransitionNode(TransitionNode node);
        #endregion

        #region ComponentNodes
        T VisitSoSNode(IntermediateSoSNode node);
        T VisitComponentDiagramNode(ComponentDiagramNode node);
        T VisitComponentNode(ComponentNode node);
        T VisitConnectorNode(ConnectorNode node);
        T VisitEntityNode(EntityNode node);

        #endregion

        #region DTONodes
        T VisitAttributeNode(AttributeNode node);
        T VisitDTONode(IntermediateDTONode node);
        T VisitTypeNode(IntermediateTypeNode node);
        #endregion

        #region ICNodes
        T VisitAssociationNode(AssociationNode node);
        T VisitClassNode(ClassNode node);
        T VisitConnectionEndNode(ConnectionEndNode node);
        T VisitConstraintNode(ConstraintNode node);
        T VisitInternalClassNode(InternalClassNode node);
        T VisitOperationNode(OperationNode node);
        T VisitParameterNode(ParameterNode node);
        T VisitPropertyNode(PropertyNode node);
        T VisitTypedNode(TypedNode node);
        #endregion
    }
}