﻿using System;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;

namespace XMLTester.SemanticAnalysis
{
    public interface IASTVisitor<out T>
    {
        // Structural nodes
        public T Visit(ModelNode node);
        public T Visit(SoSNode node);
        public T Visit(LinkNode node);
        public T Visit(ConstituentNode node);
        public T Visit(EntityNode node);
        public T Visit(ReferenceNode node);

        // Behavioural nodes
        public T Visit(ControllerNode node);
        public T Visit(EventDeclNode node);
        public T Visit(ActionGraphTreeRootNode node);
        public T Visit(ActionTransitionNode node);
        public T Visit(CommunicatorNode node);
        public T Visit(MachineNode node);
        public T Visit(InvocationGraphTreeRootNode node);
        public T Visit(InvocationTransitionNode node);

        // Data nodes
        public T Visit(DTONode node);
        public T Visit(CLSNode node);
        public T Visit(PrimitiveTypeNode node);
        public T Visit(CollectionTypeNode node);
        public T Visit(TupleTypeNode node);
        public T Visit(UserDefTypeNode node);
    }
}
