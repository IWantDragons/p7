using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SymbolTables;

namespace XMLTester.SemanticAnalysis
{
    public class TypeChecker : IASTVisitor<List<string>>
    {
        public List<string> Visit(ModelNode node)
        {
            List<string> errors = new List<string>();

            foreach (ModelNode SoS in node.Dependencies)
            {
                errors.AddRange(SoS.Accept(this));
            }

            // Now the dependencies are cool and good
            errors.AddRange(node.MainSoS.Accept(this));

            return errors;
        }

        public List<string> Visit(SoSNode node)
        {
            List<string> errors = new();

            foreach(SubSystemNode sub in node.SubSystems)
            {
                errors.AddRange(sub.Accept(this));
            }

            return errors;

        }

        public List<string> Visit(LinkNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(ConstituentNode node)
        {
            List<string> errors = new List<string>();

            foreach (SubSystemNode sub in node.SubSystems)
            {
                errors.AddRange(sub.Accept(this));
            }
            return errors;
        }

        public List<string> Visit(EntityNode node)
        {
            List<string> errors = new();
            foreach (CommunicatorNode com in node.Communicators)
            {
                errors.AddRange(com.Accept(this));
            }
            return errors;
        }

        public List<string> Visit(ReferenceNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(ControllerNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(EventDeclNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(ActionGraphTreeRootNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(ActionTransitionNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(CommunicatorNode node)
        {
            List<string> errors = new();

            List<EventDeclNode> events = (node.Scope.Parent.ASTNode as EntityNode).Controller.EventDeclarations.Where(x => x.EventType != EventTypes.Function 
                          && x.Name == node.EventName).ToList();
            if (events.Count() != 1)
            {
                //TODO: 
                errors.Add($"There are either no events or ambiguously at Entity: {node.EventName}");
            }

            EventDeclNode ev = events.ElementAt(0);

            node.Machines.ElementAt(0).InputTypes = ev.InputTypes;
            errors.AddRange(node.Machines[0].Accept(this));
            foreach (MachineNode mac in node.Machines.Skip(1))
            {
                if (mac.InputTypes != null && mac.OutputTypes != null)
                {
                    continue;
                }
                errors.AddRange(mac.Accept(this));
                //TODO
            }
            if (ev.OutputTypes.Count == 1 && (ev.OutputTypes[0] as PrimitiveTypeNode).Type == PrimitiveTypes.Void)
            {
                if (node.Machines[0].OutputTypes.Count != 0)
                    errors.Add($"Expected no output variables for {ev.Name}, but rececived {node.Machines[0].OutputTypes.Count}");
            }
            else
            {
                for (int i = 0; i < ev.OutputTypes.Count; i++)
                {
                    if (!ev.OutputTypes[i].Compare(node.Machines[0].OutputTypes[i]))
                    {
                        errors.Add($"Type of output variable nr. {i} in machine: {node.Machines[0].MachineName} does not correspond to the ones in the event declaretion: {ev.Name}\n" +
                            $"Output: {node.Machines[0].OutputTypes[i]} - event: {ev.OutputTypes[i]}");
                    }
                }
            }
            return errors;
        }

        public List<string> Visit(MachineNode node)
        {
            List<string> errors = new();
            errors.AddRange(node.GraphTreeRoots.SelectMany(root => TypeCheckTransitions(node, root, root.InvokationBranches)).ToList());
            return errors;
        }

        private List<string> TypeCheckTransitions(MachineNode node, InvocationGraphTreeRootNode IGTN, IEnumerable<InvocationTransitionNode> invokations)
        {
            List<string> errors = new();
            foreach (InvocationTransitionNode ITN in invokations)
            {
                //Input Transition
                if (node.GraphTreeRoots[0].InvokationBranches[0] == ITN && ITN.InvokationOperation is InputOutputOperation inputOperation)
                {
                    int arity = inputOperation.InputOutputVariables.Count;
                    if (node.InputTypes.Count() != arity)
                    {
                     
                        return new List<string>
                            {
                                $"The amount of inputs {arity} does not correspond in: {node.MachineName}. Expected {node.InputTypes.Count}"
                            };

                    }
                    for (int i = 0; i < arity; i++)
                    {
                        string Input = inputOperation.InputOutputVariables[i];
                        Symbol symbol = node.Scope.NewSymbol(Input, SymbolTypes.Variable);
                        TypeNode type = node.InputTypes[i];
                        if (!(node.Scope.Parent.Parent.ASTNode as EntityNode).ContainsValidTypes(type))
                        {
                            errors.Add("Non-available type " + type.ToString() + " encountered for variable " + Input + " in " + node.Scope.GetFullyQualifiedName());
                        }
                        symbol.Attributes.Add("Type", type);
                    }
                }
                //Output Transition
                else if (node.GraphTreeRoots.Last() == IGTN && ITN.InvokationOperation is InputOutputOperation outputOperation)
                {
                    List<TypeNode> Output = new();
                    foreach (string variable in outputOperation.InputOutputVariables)
                    {
                        Output.Add(node.Scope.GetSymbol(variable).Attributes["Type"] as TypeNode);
                    }
                    node.OutputTypes = Output;
                }
                //Middle Transitions
                else
                {
                    if (ITN.InvokationOperation is InvokeEventOperation ev)
                    {
                        string link = (node.Scope.Parent.ASTNode as CommunicatorNode).LinkID;
                        EventDeclNode eventDecl = null;
                        EntityNode ent = node.Scope.Parent.Parent.ASTNode as EntityNode;
                        bool isBoundary = ent.Parent is ConstituentNode cons ? cons.BoundaryEntityIDs.Contains(ent.ID) : (ent.Parent as SoSNode).BoundaryEntityIDs.Contains(ent.ID);
                        if (node.Scope.Parent.Parent.Parent.ASTNode is ConstituentNode con)
                        {
                            IEnumerable<LinkNode> links = con.Links;
                            if (isBoundary)
                            {
                                if (node.Scope.Parent.Parent.Parent.Parent.ASTNode is ConstituentNode Conparent)
                                    links = links.Concat(Conparent.Links);
                                if (node.Scope.Parent.Parent.Parent.Parent.ASTNode is SoSNode Sosparent)
                                    links = links.Concat(Sosparent.Links);
                                else
                                    throw new Exception("Unknown parent subsystem for " + con.ID);
                            }
                            IEnumerable<LinkNode> availableLinks = links.Where(x => x.ID == link).ToList();
                            if (availableLinks.Count() == 0)
                                errors.Add("No available link in either parent or parent.parent with Link id " + link);
                            else if (availableLinks.ElementAt(0).Connections.Any(x => x.EntityID.Split(".").Last() == ev.EntityName))
                            {
                                var decls = (node.Scope.Parent.Parent.Parent.GetScope(ev.EntityName).ASTNode as EntityNode).Controller.EventDeclarations.Where(x => x.Name == ev.HandlerName);
                                if (decls.Count() == 0)
                                    errors.Add($"No event with name {ev.HandlerName} in {ev.EntityName}");
                                else
                                    eventDecl = decls.ElementAt(0);
                            }
                            else
                            {
                                throw new InvalidOperationException($"There exist no entity with the name: {ev.EntityName}");
                            }
                        }
                        else if (node.Scope.Parent.Parent.Parent.ASTNode is SoSNode sos)
                        {
                            IEnumerable<LinkNode> links = sos.Links;
                            IEnumerable<LinkNode> availableLinks = links.Where(x => x.ID == link).ToList();
                            if (availableLinks.Count() == 0)
                                errors.Add("No available link in either parent or parent.parent with Link id " + link);
                            else {                              
                                if (availableLinks.ElementAt(0).Connections.Any(x => x.EntityID.Split(".").Last() == ev.EntityName))
                                {
                                    var decls = (node.Scope.Parent.Parent.Parent.GetScope(ev.EntityName).ASTNode as EntityNode).Controller.EventDeclarations.Where(x => x.Name == ev.HandlerName);
                                    if (decls.Count() == 0)
                                        errors.Add($"No event with name {ev.HandlerName} in {ev.EntityName}");
                                    else
                                        eventDecl = decls.ElementAt(0);
                                }
                                else
                                {
                                    throw new InvalidOperationException($"There exist no entity with the name: {ev.EntityName}");
                                }
                            }
                        }
                        else
                        {
                            //TODO
                            throw new InvalidOperationException($"The type of {node.Scope.Parent.Parent.Parent.Name} could not be identified");
                        }
                        if (eventDecl == null)
                            throw new InvalidOperationException($"Event declaration could not be found for invokation {ev.EntityName}.{ev.HandlerName} in {node.MachineName}");
                        if (ITN.InputVariables.Count != eventDecl.InputTypes.Count)
                        {
                            //TODO
                            errors.Add($"The amount of input variables in invokation: {ev.InvokationName} does not correspond to the amounts in machine: {node.MachineName}");
                        }
                        else
                        {
                            for (int i = 0; i < ITN.InputVariables.Count; i++)
                            {
                                if (!eventDecl.InputTypes[i].Compare(node.Scope.GetSymbol(ITN.InputVariables[i]).Attributes["Type"] as TypeNode))
                                {
                                    //TODO
                                    errors.Add($"The input type: {eventDecl.InputTypes[i]} does not match the ones in {node.Scope.GetSymbol(ITN.InputVariables[i])}");
                                }
                            }
                            for (int i = 0; i < ITN.OutputVariables.Count; i++)
                            {
                                string variable = ITN.OutputVariables[i];
                                if (node.Scope.HasSymbol(variable))
                                {
                                    //TODO
                                    errors.Add($"The variable: {ITN.OutputVariables[i]} already exists in {node.MachineName}");
                                }
                                else
                                {
                                    Symbol symbol = node.Scope.NewSymbol(variable, SymbolTypes.Variable);
                                    TypeNode type = eventDecl.OutputTypes[i];
                                    if (!(node.Scope.Parent.Parent.ASTNode as EntityNode).ContainsValidTypes(type))
                                    {
                                        errors.Add("Non-available type " + type.ToString() + " encountered for variable " + variable + " in " + node.Scope.GetFullyQualifiedName());
                                    }
                                    symbol.Attributes.Add("Type", type);
                                }

                            }

                        }
                    }
                    else if (ITN.InvokationOperation is InvokeMachineOperation mac)
                    {
                        List<TypeNode> Input = new();
                        foreach (string variable in ITN.InputVariables)
                        {
                            Input.Add(node.Scope.GetSymbol(variable).Attributes["Type"] as TypeNode);
                        }
                        MachineNode invoked = node.Scope.Parent.Scopes[mac.InvokationName].ASTNode as MachineNode;
                        invoked.InputTypes = Input;
                        errors.AddRange(invoked.Accept(this));
                        if (ITN.OutputVariables.Count != invoked.OutputTypes.Count)
                        {
                            //TODO
                            throw new Exception($"The output variables of machine: {invoked.MachineName} does not match the transition: {mac.InvokationName}");
                        }
                        for (int i = 0; i < ITN.OutputVariables.Count; i++)
                        {
                            string variable = ITN.OutputVariables[i];
                            if (node.Scope.HasSymbol(variable))
                            {
                                //TODO
                                errors.Add($"The output variable: {variable} already exists in {mac.InvokationName}");
                            }
                            else
                            {
                                Symbol symbol = node.Scope.NewSymbol(variable, SymbolTypes.Variable);
                                TypeNode type = invoked.OutputTypes[i];
                                if (!(node.Scope.Parent.Parent.ASTNode as EntityNode).ContainsValidTypes(type))
                                {
                                    errors.Add("Non-available type " + type.ToString() + " encountered for variable " + variable + " in " + node.Scope.GetFullyQualifiedName());
                                }
                                symbol.Attributes.Add("Type", type);
                            }

                        }
                    }
                    else if (ITN.InvokationOperation is InvokeFunctionOperation func)
                    {
                        List<TypeNode> InputTypes = new();

                        foreach (string variable in ITN.InputVariables)
                        {
                            InputTypes.Add(node.Scope.GetSymbol(variable).Attributes["Type"] as TypeNode);
                        }

                        ControllerNode controller = (node.Scope.Parent.Parent.ASTNode as EntityNode).Controller;
                        EventDeclNode function = new(EventTypes.Function, func.InvokationName, InputTypes, func.OutputTypes);

                        controller.EventDeclarations = controller.EventDeclarations.ToList().Concat(new List<EventDeclNode> { function }).ToList();
                        for (int i = 0; i < ITN.OutputVariables.Count; i++)
                        {
                            string variable = ITN.OutputVariables[i];
                            if (node.Scope.HasSymbol(variable))
                            {
                                errors.Add($"The output: {variable} already exists in {func.InvokationName}");
                            }
                            else
                            {
                                Symbol symbol = node.Scope.NewSymbol(variable, SymbolTypes.Variable);

                                TypeNode type = func.OutputTypes[i];
                                if (!(node.Scope.Parent.Parent.ASTNode as EntityNode).ContainsValidTypes(type))
                                {
                                    errors.Add("Non-available type " + type.ToString() + " encountered for variable " + variable + " in " + node.Scope.GetFullyQualifiedName());
                                }
                                symbol.Attributes.Add("Type", type);
                            }

                        }
                    }
                    else
                    {
                        //TODO
                        errors.Add($"The operation type for operation: {ITN.InvokationOperation.InvokationName} is not known");
                    }
                }
                errors.AddRange(TypeCheckTransitions(node,IGTN,ITN.InvokationBranches));
            }
            return errors;
        }

        public List<string> Visit(InvocationGraphTreeRootNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(InvocationTransitionNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(DTONode node)
        {
            // Check propagations?
            throw new System.NotImplementedException();
        }

        public List<string> Visit(CLSNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(PrimitiveTypeNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(CollectionTypeNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(TupleTypeNode node)
        {
            throw new System.NotImplementedException();
        }

        public List<string> Visit(UserDefTypeNode node)
        {
            throw new System.NotImplementedException();
        }
    }
}