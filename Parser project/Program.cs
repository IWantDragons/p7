using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using XMLTester.Code_Gen;
using XMLTester.Parsing;
using XMLTester.SemanticAnalysis;
using XMLTester.UppaalGen;

namespace XMLTester
{
    public class Program
    {

        public static void Main(string[] args)
        {
            ACE_It_Up(args);
        }

        static public bool ACE_It_Up(string[] args)
        {
            string VERIFIER_PATH = args.Length == 0 ? "/Applications/UPPAAL/bin-Darwin/verifyta" : args[1];
            string OPENAPIGEN_PATH = args.Length == 0 ? "../../../../openapi-generator-cli-4.3.1.jar" : args[2];

            List<string> errors = new List<string>();


            IntermediateNodes.IntermediateSoSNode sos;
            using (StreamReader umlstream = new StreamReader(args.Length == 0 ? "..//..//../../PapyrusModel/ExampleModels/SoSModel.uml" : args[0]))
                sos = new Parser(umlstream).Parse();

            Intermediate2ASTVisitor visitor = new Intermediate2ASTVisitor();
            AST.ModelNode model = (AST.ModelNode)sos.Accept(visitor);
            try
            {
                TypePropagator typePropagator = new();
                while (model.Accept(typePropagator)) ;
            }catch(Exception e)
            {
                errors.Add(e.Message);
            }
            if (!errors.Any())
            {
                TypeChecker typeChecker = new();
                errors.AddRange(model.Accept(typeChecker));
                if (!errors.Any())
                {
                    UppaalGenerator uppaalGenerator = new UppaalGenerator();
                    UppaalModel uppaalModel = uppaalGenerator.ComputeUppaalModel(model);
                    errors.AddRange(RunUppaalVerifyer(uppaalModel, VERIFIER_PATH));
                    if (!errors.Any())
                    {
                        CodeGeneratorVisitor codegen = new CodeGeneratorVisitor(OPENAPIGEN_PATH);
                        errors.AddRange(model.Accept(codegen));
                    }
                }
            }
            if (errors.Count > 0)
            {
                // Write errors to file in WebInterface/Client/resources
                if (File.Exists("../../WebInterface/Client/wwwroot/resources/error.txt"))
                    File.Delete("../../WebInterface/Client/wwwroot/resources/error.txt");
                using (StreamWriter writer = new StreamWriter(File.Create("../../WebInterface/Client/wwwroot/resources/errors.txt")))
                {
                    foreach (string error in errors)
                    {
                        writer.Write(error + "\n");
                    }
                    writer.Flush();
                }
                return false;
            }
            else
            {
                if (args.Length != 0) {
                    if (File.Exists("../../WebInterface/Client/wwwroot/resources/Model.zip"))
                        File.Delete("../../WebInterface/Client/wwwroot/resources/Model.zip");
                    ZipFile.CreateFromDirectory(@"..\..\out", @"..\..\WebInterface\Client\wwwroot\resources\Model.zip", CompressionLevel.Fastest, false);
                }
                else
                {
                    // Write zip to file file in WebInterface/Client/resources
                    if (File.Exists("../../Model.zip"))
                        File.Delete("../../Model.zip");
                    ZipFile.CreateFromDirectory(@"..\..\out", @"..\..\Model.zip", CompressionLevel.Fastest, false);
                }
                return true;
            }
        }

        private static List<string> RunUppaalVerifyer(UppaalModel model, string verifierPath)
        {
            Directory.CreateDirectory("temp");
            File.WriteAllText("temp/model.xml", model.FullModel);
            File.WriteAllText("temp/queries.q", model.Queries);
             
            Process process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = verifierPath,
                    Arguments = $"\"{Path.GetFullPath("temp/model.xml")}\" \"{Path.GetFullPath("temp/queries.q")}\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            process.Start();

            List<string> allQueries = model.Queries.Split('\n').Where(line => !string.IsNullOrEmpty(line) && line[0] != '/' && line[0] != '*').ToList();
            List<string> allErrors = new List<string>();
            while (!process.StandardOutput.EndOfStream)
            {
                string line = process.StandardOutput.ReadLine().Replace("\u001b[2K", "");
                if (!line.StartsWith(" -- "))
                    continue;

                if (line.Contains("NOT")) // That is: " -- Formula is NOT satisfied."
                    allErrors.Add($"Failed UPPAAL query: {allQueries.First()}");

                allQueries.RemoveAt(0);
            }

            return allErrors;
        }
    }
}
