﻿using System;
using System.Collections.Generic;
using XMLTester.AST.Structural;
using XMLTester.SemanticAnalysis;
using XMLTester.SymbolTables;

namespace XMLTester.AST
{
    public class ModelNode : IASTNode
    {
        public SoSNode MainSoS { get; }

        public IReadOnlyList<ModelNode> Dependencies { get; }

        public SymbolTable Scope { get; }


        public ModelNode(SoSNode mainSoS, IReadOnlyList<ModelNode> dependencies, SymbolTable modelScope)
        {
            MainSoS = mainSoS;
            Dependencies = dependencies;
            Scope = modelScope;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
