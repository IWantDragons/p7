﻿using System.Collections.Generic;
using XMLTester.AST.Data.Types;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Behavioural
{
    public class InvocationTransitionNode : IASTNode
    {
        public IReadOnlyList<string> InputVariables { get; }

        public Operation InvokationOperation { get; }

        public IReadOnlyList<string> OutputVariables { get; }


        public string EndState { get; }

        public List<InvocationTransitionNode> InvokationBranches { get; }


        public InvocationTransitionNode(IReadOnlyList<string> _inputVariables, Operation _invokationOperation, IReadOnlyList<string> _outputVariables, string _endState, List<InvocationTransitionNode> _invokationBranches)
        {
            InputVariables = _inputVariables;
            InvokationOperation = _invokationOperation;
            OutputVariables = _outputVariables;
            EndState = _endState;
            InvokationBranches = _invokationBranches;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }


    public abstract class Operation
    {
        public string InvokationName { get; }

        public Operation(string name)
        {
            InvokationName = name;
        }
    }

    public class InvokeEventOperation : Operation
    {
        public const uint BROADCAST_ISLR = 0;

        public string HandlerName { get; }
        public uint InstanceSelector { get; }
        public string EntityName { get; }

        public InvokeEventOperation(string name)
            : base(name)
        {
            string[] parts = name.Split('.');

            HandlerName = parts[2];
            InstanceSelector = parts[1] == "*" ? BROADCAST_ISLR : uint.Parse(parts[1]);
            EntityName = parts[0];
        }
    }

    public class InvokeMachineOperation : Operation {
        public InvokeMachineOperation(string name) : base(name) { }
    }

    public class InvokeFunctionOperation : Operation
    {
        public IReadOnlyList<TypeNode> OutputTypes { get; }


        public InvokeFunctionOperation(string name, IReadOnlyList<TypeNode> _outoutTypes)
            : base(name)
        {
            OutputTypes = _outoutTypes;
        }
    }

    public class InputOutputOperation : Operation
    {
        public IReadOnlyList<string> InputOutputVariables { get; set; }


        public InputOutputOperation(IReadOnlyList<string> inputOutputVariables)
            : base(null)
        {
            InputOutputVariables = inputOutputVariables;
        }
    }
}