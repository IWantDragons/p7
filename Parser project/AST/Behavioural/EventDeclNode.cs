﻿using System;
using System.Collections.Generic;
using XMLTester.AST.Data.Types;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Behavioural
{
    public class EventDeclNode : IASTNode
    {
        public EventTypes EventType { get; }

        public string Name { get; }

        public IReadOnlyList<TypeNode> InputTypes { get; }

        public IReadOnlyList<TypeNode> OutputTypes { get; }


        public EventDeclNode(EventTypes _eventType, string _name, IReadOnlyList<TypeNode> _inputTypes, IReadOnlyList<TypeNode> _outputTypes)
        {
            EventType = _eventType;
            Name = _name;
            InputTypes = _inputTypes;
            OutputTypes = _outputTypes;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }

    public enum EventTypes
    {
        Initiator, Handler, Function
    }
}
