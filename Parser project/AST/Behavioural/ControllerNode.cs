﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Behavioural
{
    public class ControllerNode : IASTNode
    {
        public IReadOnlyList<string> States { get; }

        public IReadOnlyList<EventDeclNode> EventDeclarations { get; set; }

        public IReadOnlyList<ActionGraphTreeRootNode> GraphTreeRoots { get; }



        public ControllerNode(IReadOnlyList<string> _states, IReadOnlyList<EventDeclNode> _eventDeclarations, IReadOnlyList<ActionGraphTreeRootNode> _graphTreeRoots)
        {
            States = _states;
            EventDeclarations = _eventDeclarations;
            GraphTreeRoots = _graphTreeRoots;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
