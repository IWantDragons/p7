﻿using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.AST.Data.Types;
using XMLTester.SemanticAnalysis;
using XMLTester.SymbolTables;

namespace XMLTester.AST.Behavioural
{
    public class CommunicatorNode : IASTNode
    {
        public string EventName { get; }

        public string LinkID { get; }

        public IReadOnlyList<MachineNode> Machines { get; }

        public SymbolTable Scope { get; set; }


        public CommunicatorNode(string _eventName, string _linkID, IReadOnlyList<MachineNode> _machines)
        {
            EventName = _eventName;
            LinkID = _linkID;
            Machines = _machines;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }

        public List<string> GetHandlerCalls()
        {
            return Machines
                .SelectMany(mac => mac.HandlerCalls)
                .ToList();
        }
    }
}
