﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Behavioural
{
    public class InvocationGraphTreeRootNode : IASTNode
    {
        public string RootState { get; }

        public IReadOnlyList<InvocationTransitionNode> InvokationBranches { get; }

        public InvocationGraphTreeRootNode(string _root, IReadOnlyList<InvocationTransitionNode> _branches)
        {
            RootState = _root;
            InvokationBranches = _branches;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
