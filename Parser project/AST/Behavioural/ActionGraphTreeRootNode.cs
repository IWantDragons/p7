﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Behavioural
{
    public class ActionGraphTreeRootNode : IASTNode
    {
        public string RootState { get; }

        public IReadOnlyList<ActionTransitionNode> ActionBranches { get; }


        public ActionGraphTreeRootNode(string _rootState, IReadOnlyList<ActionTransitionNode> _actionBranches)
        {
            RootState = _rootState;
            ActionBranches = _actionBranches;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
