﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Behavioural
{
    public class ActionTransitionNode : IASTNode
    {
        public const string EPSILON_ACTION = "ε";

        public const string ERROR_ACTION = "!";


        public string ActionName { get; } //DEV

        public string TimedStateID { get; } //S_ACT

        public Comparators Comparator { get; } //CMP

        public IReadOnlyList<Tuple<uint, Units>> Durations { get; } //n UNIT


        public string EndState { get; } //S_ATS11

        public List<ActionTransitionNode> ActionBranches { get; }


        public ActionTransitionNode(string _actionName, string _timedStateID, Comparators _comparator, IReadOnlyList<Tuple<uint, Units>> _durations, string _endState, List<ActionTransitionNode> _actionBranches)
        {
            ActionName = _actionName;
            TimedStateID = _timedStateID;
            Comparator = _comparator;
            Durations = _durations;

            EndState = _endState;
            ActionBranches = _actionBranches;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }

    public enum Comparators
    {
        Less, LessEq, Eq, GreaterEq, Greater
    }

    public enum Units
    {
        // These units MUST be sorted from larger to smaller, from left to right.
        Day, Hour, Minute, Second, Millisecond, Microsecond
    }
}
