﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Structural
{
    public class LinkNode : IASTNode
    {
        public string ID { get; }

        public List<LinkConnection> Connections { get; }


        public LinkNode(string _id, List<LinkConnection> _connections)
        {
            ID = _id;
            Connections = _connections;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }

    public class LinkConnection
    {
        public string EntityID { get; }

        /// <summary>
        /// '*' is represented by a '0'
        /// </summary>
        public uint Multiplicity { get; }

        public ConnectionTypes ConnectionType { get; }


        public LinkConnection(string _entityID, uint _multiplicity, ConnectionTypes _connectionType)
        {
            EntityID = _entityID;
            Multiplicity = _multiplicity;
            ConnectionType = _connectionType;
        }
    }

    public enum ConnectionTypes
    {
        Transceiver, Transponder
    }
}
