﻿using System;
using System.Collections.Generic;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Structural
{
    public class SoSNode : IASTNode
    {
        public string ID { get; }

        public IReadOnlyList<string> BoundaryEntityIDs { get; }

        public IReadOnlyList<SubSystemNode> SubSystems { get; }

        public IReadOnlyList<LinkNode> Links { get; }


        public SoSNode(string _id, IReadOnlyList<string> _boundaryEntityIDs, IReadOnlyList<SubSystemNode> _subSystems, IReadOnlyList<LinkNode> _links)
        {
            ID = _id;
            BoundaryEntityIDs = _boundaryEntityIDs;
            SubSystems = _subSystems;
            Links = _links;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
