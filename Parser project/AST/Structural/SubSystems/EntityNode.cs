﻿using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.SemanticAnalysis;
using XMLTester.SymbolTables;

namespace XMLTester.AST.Structural.SubSystems
{
    public class EntityNode : SubSystemNode
    {
        public IReadOnlyList<DTONode> DTOs { get; }

        public IReadOnlyList<CLSNode> InternalClasses { get; }

        public IReadOnlyList<CommunicatorNode> Communicators { get; }

        public ControllerNode Controller { get; }

        public SymbolTable Scope { get; set; }


        public EntityNode(string papID, string id, int count, IReadOnlyList<DTONode> dtos, IReadOnlyList<CLSNode> internalClasses, ControllerNode controller, IReadOnlyList<CommunicatorNode> communicators)
            : base(papID, id, count)
        {
            DTOs = dtos;
            InternalClasses = internalClasses;
            Controller = controller;
            Communicators = communicators;
        }

        List<string> DTONames = null;

        public bool ContainsValidTypes(TypeNode type)
        {
            if(DTONames == null)
                DTONames = DTOs.Select(dto => dto.ID).Concat(Scope.PropagatedSymbols.Select(propagate => (propagate.Symbol.ASTNode as DTONode).ID)).ToList();
            if (type is PrimitiveTypeNode)
            {
                return true;
            }
            //Check if the type is a tuple.
            else if (type is TupleTypeNode tuple)
            {
                bool res = true;
                foreach (TypeNode elem in tuple.ElementTypes)
                {
                    res = res && ContainsValidTypes(elem);
                }
                return res;
            }
            else if (type is CollectionTypeNode collec)
            {
                return ContainsValidTypes(collec.ElementType);
            }
            else if (type is UserDefTypeNode user)
            {
                return DTONames.Contains(user.UserTypeID);
            }
            else
                throw new InvalidOperationException("Unknown TypeNode encountered " + type.ToString());
        }


        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
