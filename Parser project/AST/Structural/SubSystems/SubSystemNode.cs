﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Structural.SubSystems
{
    public abstract class SubSystemNode : IASTNode
    {
        public const int COUNT_KNOWN_MANY = 0;
        public const int COUNT_UNKNOWN_MANY = -1;

        public string PapID { get; }

        public string ID { get; }

        public int Count { get; }

        public IASTNode Parent { get; set; }


        protected SubSystemNode(string papID, string id, int count)
        {
            PapID = papID;
            ID = id;
            Count = count;
        }


        public abstract T Accept<T>(IASTVisitor<T> visitor);
    }
}
