﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Structural.SubSystems
{
    public class ReferenceNode : SubSystemNode
    {
        public string ReferencedSoSID { get; }

        public ReferenceModes Mode { get; }


        public ReferenceNode(string papID, string id, int count, string referencedSoSid, ReferenceModes mode)
            : base(papID, id, count)
        {
            ReferencedSoSID = referencedSoSid;
            Mode = mode;
        }


        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }

    public enum ReferenceModes
    {
        Local, Global
    }
}
