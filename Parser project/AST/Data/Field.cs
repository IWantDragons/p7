﻿using System;
using XMLTester.AST.Data.Types;

namespace XMLTester.AST.Data
{
    public class Field
    {
        public TypeNode Type { get; }

        public string Name { get; }

        public string Visibility { get; }


        public Field(TypeNode type, string name, string visibility = null)
        {
            Type = type;
            Name = name;
            Visibility = visibility;
        }
    }
}
