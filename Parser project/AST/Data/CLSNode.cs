﻿using System;
using System.Collections.Generic;
using XMLTester.AST.Data.Types;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data
{
    public class CLSNode : IASTNode
    {
        public string ID { get; }

        public string Base { get; set; }

        public IReadOnlyList<Field> Fields { get; }

        public IReadOnlyList<Operation> Operations { get; }

        public IReadOnlyList<Constraint> OCLs { get; }


        public CLSNode(string _id, string _base, IReadOnlyList<Field> _fields, IReadOnlyList<Operation> _operations, IReadOnlyList<Constraint> _ocls)
        {
            ID = _id;
            Base = _base;
            Fields = _fields;
            Operations = _operations;
            OCLs = _ocls;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }

    public class Operation
    {
        public TypeNode ReturnType { get; }

        public IReadOnlyDictionary<string, TypeNode> InputParameters { get; }

        public string Name { get; }

        public string Visibility { get; }


        public Operation(TypeNode returnType, IReadOnlyDictionary<string, TypeNode> inputParameters, string name, string visibility)
        {
            ReturnType = returnType;
            InputParameters = inputParameters;
            Name = name;
            Visibility = visibility;
        }
    }
}
