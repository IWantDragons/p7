﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data.Types
{
    public class TupleTypeNode : TypeNode
    {
        public IReadOnlyList<TypeNode> ElementTypes { get; }


        public TupleTypeNode(IReadOnlyList<TypeNode> _elementTypes)
        {
            ElementTypes = _elementTypes;
        }

        public override string ToString()
        {
            string res = "(";
            foreach(TypeNode type in ElementTypes)
            {
                res += type.ToString() + " ";
            }
            return res + ")";
        }


        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }

        public override bool Compare(TypeNode type)
        {
            if (!(type is TupleTypeNode))
                return false;
            else
            {
                TupleTypeNode tuple = type as TupleTypeNode;
                if (this.ElementTypes.Count != tuple.ElementTypes.Count)
                    return false;
                else
                {
                    for(int i = 0; i < ElementTypes.Count; ++i)
                    {
                        if (!ElementTypes[i].Equals(tuple.ElementTypes[i]))
                            return false;
                    }
                    return true;
                }
                
            }

        }
    }
}
