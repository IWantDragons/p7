﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data.Types
{
    public class PrimitiveTypeNode : TypeNode
    {
        public PrimitiveTypes Type { get; }


        public PrimitiveTypeNode(PrimitiveTypes _type)
        {
            Type = _type;
        }


        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }

        public override bool Compare(TypeNode type)
        {
            if (!(type is PrimitiveTypeNode))
                return false;
            else
                return (type as PrimitiveTypeNode).Type == this.Type;

        }


        public override string ToString()
        {
            switch (Type)
            {
                case PrimitiveTypes.Bool:
                    return "bool";
                case PrimitiveTypes.Byte:
                    return "byte";
                case PrimitiveTypes.Char:
                    return "char";
                case PrimitiveTypes.Int:
                    return "int";
                case PrimitiveTypes.Real:
                    return "real";
                case PrimitiveTypes.String:
                    return "string";
                case PrimitiveTypes.ByteSequence:
                    return "byte-sequence";
                case PrimitiveTypes.Void:
                    return "void";
                default:
                    throw new Exception("Cannot get stringform of unknown type " + Type.ToString());
            }
        }
    }

    public enum PrimitiveTypes
    {
        Bool, Byte, Char, Int, Real, String,
        ByteSequence, Void
    }
}
