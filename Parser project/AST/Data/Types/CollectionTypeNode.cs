﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data.Types
{
    public class CollectionTypeNode : TypeNode
    {
        public CollectionTypes CollectionType { get; }

        public TypeNode ElementType { get; }


        public CollectionTypeNode(CollectionTypes _collectionType, TypeNode _elementType)
        {
            CollectionType = _collectionType;
            ElementType = _elementType;
        }


        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }

        public override bool Compare(TypeNode type)
        {
            if (!(type is CollectionTypeNode))
                return false;
            else
            {
                CollectionTypeNode collec = type as CollectionTypeNode;
                return collec.CollectionType == this.CollectionType && collec.ElementType.Equals(this.ElementType);
            }

        }
    }

    public enum CollectionTypes
    {
        List, Set
    }
}
