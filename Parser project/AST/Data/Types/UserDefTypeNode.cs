﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data.Types
{
    public class UserDefTypeNode : TypeNode
    {
        public string UserTypeID { get; set; }


        public UserDefTypeNode(string userTypeID)
        {
            UserTypeID = userTypeID;
        }
        

        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }

        public override bool Compare(TypeNode type)
        {
            if (!(type is UserDefTypeNode))
                return false;
            else
                return (type as UserDefTypeNode).UserTypeID == this.UserTypeID;

        }
    }
}
