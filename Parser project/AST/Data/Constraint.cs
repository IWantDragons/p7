﻿using System;
namespace XMLTester.AST.Data
{
    public class Constraint
    {
        public string Signature { get; }

        public string Value { get; }


        public Constraint(string signature, string value)
        {
            Signature = signature;
            Value = value;
        }
    }
}
