﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data
{
    public class DTONode : IASTNode
    {
        public string ID { get; }

        public string Base { get; set; }

        public IReadOnlyList<Field> Fields { get; }

        public IReadOnlyList<Constraint> OCLs { get; }


        public DTONode(string _id, string _base, IReadOnlyList<Field> _fields, IReadOnlyList<Constraint> _ocls)
        {
            ID = _id;
            Base = _base;
            Fields = _fields;
            OCLs = _ocls;
        }


        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
