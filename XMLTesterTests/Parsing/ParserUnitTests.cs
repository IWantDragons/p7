﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using XMLTester.IntermediateNodes;
using XMLTester.Parsing;

namespace XMLTesterTests.ParserClass.UnitTests
{
    [TestClass]
    public class ParserUnitTests
    {
        private const string ICTestFiles = "..\\..\\..\\testUML\\ICTestFiles\\";
        private const string SCMTestFiles = "..\\..\\..\\testUML\\SCMTestFiles\\";
        private const string FullParserTestFiles = "..\\..\\..\\TestUML\\FullParserTestFiles\\";


        [TestClass]
        public class ExpectMethod
        {
            // --- General parsing ---
            [TestMethod]
            public void ExpectSingleTag()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "expectCorrect.xml");
                Parser parser = new Parser(file);

                // Act
                bool result = parser.Expect("xml");

                // Assert
                Assert.AreEqual(true, result);
            }

            [TestMethod]
            public void ExpectFullFileUsingSingleExpect()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "expectCorrect.xml");
                Parser parser = new Parser(file);

                // Act
                bool expectXML = parser.Expect("xml");
                bool expectUMLModelOpen = parser.Expect("uml:Model");
                bool expectType = parser.Expect("type");
                bool expectOwnedOperation = parser.Expect("ownedOperation");
                bool expectUMLModelClose = parser.Expect("uml:Model");
                bool EOF = parser.reader.EOF;

                // Assert
                Assert.AreEqual(true, expectXML);
                Assert.AreEqual(true, expectUMLModelOpen);
                Assert.AreEqual(true, expectType);
                Assert.AreEqual(true, expectOwnedOperation);
                Assert.AreEqual(false, expectUMLModelClose);
                Assert.AreEqual(true, EOF);

            }

            [TestMethod]
            public void ExpectFullFileUsingManyExpect()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "expectCorrect.xml");
                Parser parser = new Parser(file);

                // Act
                bool expectAll = parser.Expect(new string[]
                { "xml", "uml:Model", "type", "ownedOperation", "uml:Model" });
                bool EOF = parser.reader.EOF;

                // Assert
                Assert.AreEqual(false, expectAll);
                Assert.AreEqual(true, EOF);

            }

            [TestMethod]
            public void ExceptionOnBadSingleExpect()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "expectCorrect.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "Expected: \"This is clearly wrong\", found: \"xml\", at line 1";

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.Expect("This is clearly wrong"));
                Assert.AreEqual(expectedMessage, ex.Message);
            }

            [TestMethod]
            public void ExceptionOnBadManyExpect()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "expectCorrect.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "Expected: \"clearly wrong\", found: \"type\", at line 3";

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.Expect(new string[] { "xml", "uml:Model", "clearly wrong" }));
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }

        [TestClass]
        public class SoSNodeMethod
        {
            [TestMethod]
            public void Successful()
            {
                // Arrange
                StreamReader file = File.OpenText(FullParserTestFiles + "FullModel.xml");
                Parser parser = new Parser(file);

                // Act
                parser.Expect(new string[] { "xml", "uml:Model" });
                IntermediateSoSNode node = parser.SoSNode();
                parser.reader.Close();

                // Assert
                Assert.AreEqual("SoS1", node.System.Name);
                Assert.AreEqual(1, node.Dependencies.Count);
                Assert.AreEqual("SoS2", node.Dependencies[0].System.Name);
            }

            [TestMethod]
            public void RedundantComponentDiagram()
            {
                // Arrange
                StreamReader file = File.OpenText(FullParserTestFiles + "FullModelRedundantComponent.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "Registered redundant component diagram declaration at line 21";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.SoSNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }

            [TestMethod]
            public void UnknownOuterElement()
            {
                // Arrange
                StreamReader file = File.OpenText(FullParserTestFiles + "FullModelUnknownOuterElement.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "Unknown outer element at line 3";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.SoSNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }

        [TestClass]
        public class InternalClassNodeMethod
        {
            [TestMethod]
            public void Successful()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "InternalClassSuccessful.xml");
                Parser parser = new Parser(file);

                // Act
                parser.Expect(new string[] { "xml", "uml:Model" });
                InternalClassNode node = parser.InternalClassNode();
                parser.Expect("uml:Model");

                // Assert
                Assert.AreEqual(2, node.Elements.Count);
                Assert.AreEqual(typeof(ClassNode), node.Elements[0].GetType());
                Assert.AreEqual(typeof(AssociationNode), node.Elements[1].GetType());
            }

            [TestMethod]
            public void Failure()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "InternalClassFailure.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "Unknown Package Element encountered at line 4";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.InternalClassNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }

        [TestClass]
        public class AssociationNodeMethod
        {
            [TestMethod]
            public void SuccessWithVisbilityNoConstraint()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestVisibility.xml");
                Parser parser = new Parser(file);

                // Act
                parser.Expect(new string[] { "xml", "uml:Model" });
                AssociationNode node = parser.AssociationNode();
                parser.Expect("uml:Model");

                // Assert
                Assert.AreEqual("AssociationNode", node.ID);
                Assert.AreEqual("package", node.Visibility);
            }

            [TestMethod]
            public void SuccessNoVisibilityWithConstraint()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestConstraint.xml");
                Parser parser = new Parser(file);

                // Act
                parser.Expect(new string[] { "xml", "uml:Model" });
                AssociationNode node = parser.AssociationNode();
                parser.Expect("uml:Model");

                // Assert
                Assert.AreEqual("AssociationNode", node.ID);
                Assert.AreEqual(1, node.Constraints.Count);
                Assert.AreEqual("Student.Age > 18", node.Constraints[0].constraint);
            }

            [TestMethod]
            public void SuccessWithVisibilityAndConstraint()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestConstraintVisibility.xml");
                Parser parser = new Parser(file);

                // Act
                parser.Expect(new string[] { "xml", "uml:Model" });
                AssociationNode node = parser.AssociationNode();
                parser.Expect("uml:Model");

                // Assert
                Assert.AreEqual("AssociationNode", node.ID);
                Assert.AreEqual("package", node.Visibility);
                Assert.AreEqual(1, node.Constraints.Count);
                Assert.AreEqual("Student.Age > 18", node.Constraints[0].constraint);
            }

            [TestMethod]
            public void SuccessDefaultLowerValue()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestDefaultLowerValue.xml");
                Parser parser = new Parser(file);

                // Act
                parser.Expect(new string[] { "xml", "uml:Model" });
                AssociationNode node = parser.AssociationNode();
                parser.Expect("uml:Model");

                // Assert
                Assert.AreEqual("AssociationNode", node.ID);
                Assert.AreEqual("X", node.ChildClassType.LowerMultiplicity);
            }

            [TestMethod]
            public void FailureIncorrectNumberOfMemberEnds()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestFailNumberOfMemberEnds.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "AssociationNode requires 2 ends, but found 3 declared, at line 3";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.AssociationNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }

            [TestMethod]
            public void FailureIncorrectNumberOfActualEnds()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestFailNumberOfActualEnds.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "AssociationNode requires 2 ends, but found 3 actual, at line 13";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.AssociationNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }

            [TestMethod]
            public void FailureNoChildClass()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestFailNoChildClass.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "ChildClassType is null for association 'AssociationNode'";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Arrange and act
                var ex = Assert.ThrowsException<ParseException>(() => parser.AssociationNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }

            [TestMethod]
            public void FailureNoContainingClass()
            {
                StreamReader file = File.OpenText(ICTestFiles + "AssociationTestFailNoContainingClass.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "ContainingClassID is null for association 'AssociationNode'";
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Arrange and act
                var ex = Assert.ThrowsException<ParseException>(() => parser.AssociationNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }

        [TestClass]
        public class ClassNodeMethod
        {
            [TestMethod]
            public void SuccessOwnedRule()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "ClassTestOwnedRule.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act
                ClassNode node = parser.ClassNode();

                // Assert
                Assert.AreEqual("TestID", node.PapID);
                Assert.AreEqual("Test", node.ClassName);
                Assert.AreEqual(1, node.Constraints.Count);
                Assert.AreEqual("Not true OCL", node.Constraints[0].constraint);
            }

            [TestMethod]
            public void SuccessOwnedAttribute()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "ClassTestOwnedAttribute.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act
                ClassNode node = parser.ClassNode();

                // Assert
                Assert.AreEqual("TestID", node.PapID);
                Assert.AreEqual("Test", node.ClassName);
                Assert.AreEqual(1, node.Variables.Count);
                
                PropertyNode propNode = node.Variables[0];
                Assert.AreEqual("id:oneliner", propNode.PapID);
                Assert.AreEqual("attribute", propNode.Signature);
                Assert.AreEqual("package", propNode.Visibility);
                Assert.AreEqual("typeTester", propNode.Type.TypeNameOrID);
            }

            [TestMethod]
            public void SuccessOwnedOperation()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "ClassTestOwnedOperation.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act
                ClassNode node = parser.ClassNode();

                // Assert
                Assert.AreEqual("TestID", node.PapID);
                Assert.AreEqual("Test", node.ClassName);
                Assert.AreEqual(1, node.Methods.Count);

                // The actual contents of the parameters will be tested in other tests
                // Specifically those that are actually for parameter nodes.
                OperationNode opNode = node.Methods[0];
                Assert.AreEqual("id:TestOperation", opNode.PapID);
                Assert.AreEqual("Test Operation", opNode.Signature);
                Assert.AreEqual("package", opNode.Visibility);
                Assert.AreEqual(2, opNode.Parameters.Count);
            }

            [TestMethod]
            public void SuccessGeneralization()
            {
                // Arrange
                StreamReader file = File.OpenText(ICTestFiles + "ClassTestGeneralization.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[] { "xml", "uml:Model" });

                // Act
                ClassNode node = parser.ClassNode();

                // Assert
                Assert.AreEqual("TestID", node.PapID);
                Assert.AreEqual("Test", node.ClassName);
                Assert.AreEqual("GeneralizationTest", node.BaseClassPapID);
            }

            [TestMethod]
            public void FailureUnknownTag()
            {
                // Arrange
                // Note that the file has a generalization before the bad tag. This is required.
                StreamReader file = File.OpenText(ICTestFiles + "ClassTestFailBadTag.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[] { "xml", "uml:Model" });
                string expectedMessage = "Encountered unknown tag \"ownedErrorTag\" when parsing class features at line: 5";

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.ClassNode());
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }

        [TestClass]
        public class OperationNodeMethod
        {

        }

        [TestClass]
        public class ParameterNodeMethod
        {

        }

        [TestClass]
        public class PropertyNodeMethod
        {

        }

        [TestClass]
        public class ConstraintNodeMethod
        {
            // --- Constraints ---
            [TestMethod]
            public void Successful()
            {
                StreamReader file = File.OpenText(ICTestFiles + "ConstraintTest1.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[]
                    { "xml", "uml:Model", "packageImport", "importedPackage", "packageImport", "packagedElement"});
                ConstraintNode node = parser.ConstraintNode();
                Assert.AreEqual(node.signature, "TestConstraint");
                Assert.AreEqual(node.ID, "12Test");
                Assert.AreEqual(node.constraint, "Test.length < 20");
            }

            [TestMethod]
            [ExpectedException(typeof(ParseException))]
            public void Failure()
            {
                StreamReader file = File.OpenText(ICTestFiles + "ConstraintTest2.xml");
                Parser parser = new Parser(file);
                // This is where the exception should be thrown.
                parser.Expect("xml");
                parser.ConstraintNode();
            }
        }

        [TestClass]
        public class EntityBehaviorMethod
        {

        }

        [TestClass]
        public class RegionNodeMethod
        {
            //[TestMethod()]
            //public void ParseRegionNode()
            //{
            //    StreamReader file = File.OpenText(testDir + "RegionTest.xml");
            //    Parser parser = new Parser(file);
            //    parser.Expect("xml");
            //    parser.Expect("uml:Model");

            //    RegionNode region = parser.RegionNode();
            //    Assert.AreEqual("Unfortunate Ending", region.CommunicatorName);
            //    Assert.AreEqual(3, region.states.Count);
            //    Assert.AreEqual(2, region.transitions.Count);

            //    parser.reader.Close();
            //}

            [TestMethod()]
            [ExpectedException(typeof(ParseException))]
            public void Failure()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "RegionTestFail.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                //This is where the exception is thrown.
                parser.RegionNode();
            }
        }

        [TestClass]
        public class StateNodeMethod
        {
            [TestMethod()]
            public void InitialState()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "InitialStateTest.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                InitialStateNode initial = (InitialStateNode)parser.StateNode();
                Assert.AreEqual("Initial State for testing", initial.StateName);

                parser.reader.Close();
            }

            [TestMethod()]
            public void Pseudostate()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "PseudoStateTest.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                StateNode fork = parser.StateNode();
                Assert.AreEqual("This is a forking state", fork.StateName);

                parser.reader.Close();
            }

            [TestMethod()]
            public void Statenode()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "StateTest.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                StateNode state = parser.StateNode();
                Assert.AreEqual("ValidState", state.StateName);

                parser.reader.Close();
            }

            [TestMethod()]
            public void FinalState()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "FinalStateTest.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                FinalStateNode final = (FinalStateNode)parser.StateNode();
                Assert.AreEqual("Final State for Testing", final.StateName);

                parser.reader.Close();
            }

            [TestMethod()]
            [ExpectedException(typeof(ParseException))]
            public void ExceptionOnStateNode()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "StateTestFail.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                //This is where the exception is thrown.
                parser.StateNode();
            }
        }

        [TestClass]
        public class TransitionNodeMethod
        {
            [TestMethod()]
            public void TransitionNode()
            {
                StreamReader file = File.OpenText(SCMTestFiles + "TransitionTest.xml");
                Parser parser = new Parser(file);
                parser.Expect("xml");
                parser.Expect("uml:Model");

                TransitionNode transition = parser.TransitionNode();
                Assert.AreEqual("dead", transition.Source);
                Assert.AreEqual("true", transition.Destination);
                Assert.AreEqual("Trans rights are human rights", transition.Content);

                parser.reader.Close();
            }
        }

        [TestClass]
        public class DTONodeMethod
        {

        }

        [TestClass]
        public class ComponentDiagramNodeMethod
        {

        }
    }
}