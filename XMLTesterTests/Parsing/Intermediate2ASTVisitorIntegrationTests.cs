﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLTester.AST;
using XMLTester.IntermediateNodes;
using XMLTester.Parsing;
using XMLTester.SemanticAnalysis;

namespace XMLTesterTests.Intermediate2AST.IntegrationTests
{
    [TestClass]
    public class Intermediate2ASTVisitorIntegrationTests
    {
        [TestClass]
        public class VisitSoSNode
        {
            [TestMethod]
            public void FullConversionSuccess()
            {
                // Arrange
                string FullParserTestFiles = "..\\..\\..\\TestUML\\FullParserTestFiles\\";
                StreamReader file = File.OpenText(FullParserTestFiles + "FullModel.xml");
                Parser parser = new Parser(file);
                IntermediateSoSNode SoS = parser.Parse();
                parser.reader.Close();
                Intermediate2ASTVisitor visitor = new Intermediate2ASTVisitor();

                // Act
                ModelNode model = (ModelNode)SoS.Accept(visitor);

                // Assert
                Assert.IsNotNull(model);
            }
        }
    }
}
