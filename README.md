# ACS prototype implementation

This repo represents a barebones prototype of an ACS framework implementation. The repo is sadly messy, as it was completed in a hurry, for the end of a semester. Travel at your own risk.

Please also note, that you may see "ACE" throughout. This was the original abbreviation for ACS, but is not applicable anymore.

The root contains various notes used throughout working, as well as the project folders. The `PapyrusModel` folder contains projects that can be opened with [Eclipse Papyrus](https://www.eclipse.org/papyrus/). `Parser project`, `WebInterface` and `XMLTesterTests` can be opened together, by opening the `Parser project\XMLTester.sln` file in your favorite Visual Studio variant.

## Contents
- `PapyrusModel`: Contains an example ACS model for Gnutella modelled in Papyrus. Does not contain a C# project. Papyrus can be downloaded from [their website](https://www.eclipse.org/papyrus/)
- `Parser project`: Contains the back-end of the internet application. Contains the Solution file "XMLTester.sln" and also a C# project "XMLTester.csproj".
- `WebInterface`: Contains the front-end of the internet application, with respect to client and server. Contains three C# projects "WebInterface.Client.csproj", "WebInterface.Server.csproj" and "WebInterface.Shared.csproj" (which is enforced by Blazor, but unused by us).
- `XMLTesterTests`: Contains the tests for the back-end of our internet application. Contains a C# project "XMLTesterTests.csproj".

## Compilation and running.

To run, you will need an installation of [UPPAAL](https://uppaal.org/).

There are two options for running the system:

- As a web-application:
  - Inside the `WebInterface\Server\Controllers\UploadController.cs` file, on line 36, replace the second argument with the path to your local UPPAAL installation's `verifiyta.exe`
  - Inside the `WebInterface\Server\Controllers\UploadController.cs` file, on line 36, replace the third argument with the path to your local `openapi-generator-cli-X.X.X.jar`. One is included in this repo.
  - Build the entire solution and launch the "WebInterface.Server.csproj" project through Visual Studio.
- just the back-end:
  - Two options (works both through Visual Studio and by launching the .exe file manually):
    - Without program arguments
      - Inside the `Parser project\Program.cs` file, on line 27, replace the string variable `VERIFIER_PATH`'s value with the path to your local UPPAAL installation's `verifiyta.exe`
      - Inside the `Parser project\Program.cs` file, on line 27, replace the string variable `OPENAPIGEN_PATH`'s value with the path to your local `openapi-generator-cli-X.X.X.jar`. One is included in this repo.
      - Inside the `Parser project\Program.cs` file, on line 34, replace the default string with the path to the `.uml` file for your ACS model.
      - Build the project and launch the "XMLTester.csproj" project's executable.
    - With program arguments
      - Build the project.
      - Run the project's `.exe` file with the following arguments:
        1. The path to your ACS model's .uml/.xml file.
        2. The path to your local UPPAAL installation's `verifiyta.exe`
        3. the path to your local `openapi-generator-cli-X.X.X.jar`. One is included in this repo.

We apologize for the complexity of running the application.
